# "SELECT shops.size AS called_size, shirt_size.* FROM shirt_size inner join shops on shops.id=shirt_size.id WHERE enable = '1'"

import pymysql
from logger_helper import InitLogger
import config
# from sklearn.cluster import DBSCAN
# import numpy as np
import pandas as pd
from pandas import DataFrame
from sklearn.cluster import DBSCAN
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import normalize
from sklearn.decomposition import PCA


IN_CHARGE_TG_ID = '@m.sadat'
log = InitLogger(logger_name="takesize_classifier")


def mysqlConnector(database_config):
    while True:
        try:
            db = pymysql.connect(host=database_config['host'],
                                 user=database_config['user'],
                                 password=database_config['password'],
                                 db=database_config['db'],
                                 charset=database_config['charset'],
                                 port=database_config['port'],
                                 cursorclass=pymysql.cursors.DictCursor,
                                 autocommit=True
                                 )
            return db
        except Exception as e:
            log.create_log_message(msg='Error on connecting to takesize_db! - {}'.format(str(e))+IN_CHARGE_TG_ID)
            print("error")
            continue
    if db.is_connected():
        print("db is connected")
    else:
        print("couldn't connect to database!")


def classify_shirts(db, table_name, batch_size):
    cursor = db.cursor()
    cursor.execute("SELECT count(*) FROM {}".format(table_name))
    count = int(cursor.fetchone()['count(*)'])
    batch_size = batch_size  # whatever

    for offset in range(0, count, batch_size):

        cursor.execute("SELECT shirt_size.* , shops.size FROM {} INNER JOIN shops ON shops.id=shirt_size.phone_number LIMIT %s OFFSET %s".format(table_name), (batch_size, offset))
        temp = cursor.fetchall()
        # X = np.array([[1, 2], [2, 2], [2, 3],
        #               ...[8, 7], [8, 8], [25, 80]])  # x = cursor.fetchall() = temp
        # clustering = DBSCAN(eps=3, min_samples=2).fit(X)
        # print(clustering.labels_)
        df = DataFrame(temp)
        X = df.drop(['phone_number', 'id','size'], axis=1)
        # Scaling the data to bring all the attributes to a comparable level
        scaler = StandardScaler()
        X_scaled = scaler.fit_transform(X)

        # Normalizing the data so that
        # the data approximately follows a Gaussian distribution
        X_normalized = normalize(X_scaled)

        # Converting the numpy array into a pandas DataFrame
        X_normalized = pd.DataFrame(X_normalized)

        pca = PCA(n_components=5)
        X_principal = pca.fit_transform(X_normalized)
        X_principal = pd.DataFrame(X_principal)
        X_principal.columns = ['P1', 'P2', 'P3', 'P4', 'P5']
        print(X_principal.head())
        print(df.head())
        # Numpy array of all the cluster labels assigned to each data point
        db_default = DBSCAN(eps=0.1, min_samples=3).fit(X_principal)
        labels = db_default.labels_

        print(labels)

        # print(df.head())
        # for row in temp:
            # print(row)

    return None



classify_shirts(mysqlConnector(config.takesize_db), "shirt_size", 50)