import logging

class InitLogger:
    def __init__(self,
                 logger_name = __name__,
                 log_path = "./general.log",
                 log_format = '%(asctime)s-File %(pathname)s-Line %(lineno)d-Method %(funcName)s-LogLevel %(levelname)s :: %(message)s',
                 log_level = logging.WARNING):

        # Gets or creates a logger
        self.logger = logging.getLogger(logger_name)

        # set log level
        self.logger.setLevel(log_level)

        # define file handler and set formatter
        file_handler = logging.FileHandler(log_path)
        formatter = logging.Formatter(log_format)
        file_handler.setFormatter(formatter)

        # add file handler to logger
        self.logger.addHandler(file_handler)


    def create_log_message(self, msg):
        self.logger.warning("{}".format(msg))

# Logs
# logger.debug('A debug message')
# logger.info('An info message')
# logger.warning('Something is not right.')
# logger.error('A Major error has happened.')
# logger.critical('Fatal error. Cannot continue')